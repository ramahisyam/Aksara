package com.example.zambrut.aksara;

public class Question {
    public static final String BAGIAN_1 = "1";
    public static final String BAGIAN_2 = "2";
    public static final String BAGIAN_3 = "3";
    public static final String BAGIAN_4 = "4";
    public static final String BAGIAN_5 = "5";

    private String question;
    private String answer;
    private int answer_nr;
    private String bagian;

    public Question(){}

    public Question(String question, String answer, int answer_nr, String bagian) {
        this.question = question;
        this.answer = answer;
        this.answer_nr = answer_nr;
        this.bagian = bagian;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getAnswer_nr() {
        return answer_nr;
    }

    public void setAnswer_nr(int answer_nr) {
        this.answer_nr = answer_nr;
    }

    public String getBagian() {
        return bagian;
    }

    public void setBagian(String bagian) {
        this.bagian = bagian;
    }

    public static String[] getAllBagians(){
        return new String[] {
                BAGIAN_1,
                BAGIAN_2,
                BAGIAN_3,
                BAGIAN_4,
                BAGIAN_5
        };
    }
}
