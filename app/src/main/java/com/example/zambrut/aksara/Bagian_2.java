package com.example.zambrut.aksara;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class Bagian_2 extends Activity {
    private TextView textQuestion2, teks2, textQuestionCount2, textAnswer2;
    private Button btnLanjutkan2;
    private EditText edtAnswer2;

    private List<Question> questionList2;
    private int questionCounter2;
    private int questionCountTotal2;
    private Question currentQuestion2;

    private boolean answered2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_bagian_2);

        textQuestion2 = (TextView) findViewById(R.id.text_question2);
        Typeface customfont = Typeface.createFromAsset(getAssets(), "fonts/aksara.TTF");
        textQuestion2.setTypeface(customfont);
        textQuestionCount2 = (TextView) findViewById(R.id.text_question_count2);
        btnLanjutkan2 = (Button) findViewById(R.id.btn_lanjutkan2);
        edtAnswer2 = (EditText) findViewById(R.id.edt_answer2);

        DBHelper dbHelper = new DBHelper(this);
        questionList2 = dbHelper.getQuestion("2");
        questionCountTotal2 = questionList2.size();

        showNextQuestion();

        btnLanjutkan2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkAnswer();
            }
        });
    }

    private void showNextQuestion(){

        if (questionCounter2 < questionCountTotal2){
            currentQuestion2 = questionList2.get(questionCounter2);

            textQuestion2.setText(currentQuestion2.getQuestion());

            questionCounter2++;
            textQuestionCount2.setText("Soal " + questionCounter2 + "/" + questionCountTotal2);
            answered2 = false;
            btnLanjutkan2.setText("Confirm");
        }else {
            finishQuiz();
        }
    }

    private void checkAnswer(){
        if (!edtAnswer2.getText().toString().isEmpty()){
//            edtAnswer.setError("Isi dengan Benar");
            if (edtAnswer2.getText().toString().equalsIgnoreCase(currentQuestion2.getAnswer())){
                Toast.makeText(this, "Jawaban Benar", Toast.LENGTH_SHORT).show();
                showNextQuestion();
            }else {
                Toast.makeText(this, "Jawaban Salah", Toast.LENGTH_SHORT).show();
            }
        }else {
            Toast.makeText(this, "Masukkan Jawaban", Toast.LENGTH_SHORT).show();
        }
    }

    private void finishQuiz(){
        finish();
    }

    public void kembali2(View view){
        finish();
    }
}
