package com.example.zambrut.aksara;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

public class MainActivity extends Activity {

    TextView teks, teks2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_main);

        teks=(TextView)findViewById(R.id.texta);
        Typeface customfont=Typeface.createFromAsset(getAssets(),"fonts/aksara.TTF");
        teks.setTypeface(customfont);

    }
    public void quiz(View v) {
        final Animation menghilang = AnimationUtils.loadAnimation(this, R.anim.anim_alpha);
        Intent intent = new Intent(MainActivity.this, Quiz.class);
        startActivity(intent);
        v.startAnimation(menghilang);
    }
    public void aksara(View v) {
        final Animation menghilang = AnimationUtils.loadAnimation(this, R.anim.anim_alpha);
        Intent intent = new Intent(MainActivity.this, Belajar_aksara.class);
        startActivity(intent);
        v.startAnimation(menghilang);
    }
    public void terjemahan(View v) {
        final Animation menghilang = AnimationUtils.loadAnimation(this, R.anim.anim_alpha);
        Intent intent = new Intent(MainActivity.this, Terjemahan.class);
        startActivity(intent);
        v.startAnimation(menghilang);
    }
    public void tentang(View v) {
        final Animation menghilang = AnimationUtils.loadAnimation(this, R.anim.anim_alpha);
        v.startAnimation(menghilang);
    }
}
