package com.example.zambrut.aksara;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "Aksara.db";
    private static final int DATABASE_VERSION = 1;

    private SQLiteDatabase db;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        this.db = sqLiteDatabase;

        final String SQL_CREATE_QUESTION_TABLE = "CREATE TABLE " +
                DBContract.DBTable.TABLE_NAME + " ( " +
                DBContract.DBTable._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DBContract.DBTable.COLUMN_QUESTION + " TEXT, " +
                DBContract.DBTable.COLUMN_ANSWER + " TEXT, " +
                DBContract.DBTable.COLUMN_ANSWER_NR + " INTEGER, " +
                DBContract.DBTable.COLUMN_BAGIAN + " TEXT" +
                ")";
        sqLiteDatabase.execSQL(SQL_CREATE_QUESTION_TABLE);
        fillQuestionTable();
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DBContract.DBTable.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    private void fillQuestionTable(){
        Question q1 = new Question("Kyai", "kyai", 1, Question.BAGIAN_1);
        addQuestion(q1);
        Question q2 = new Question("Bima", "bima", 2, Question.BAGIAN_1);
        addQuestion(q2);
        Question q3 = new Question("Baladewa", "baladewa", 3, Question.BAGIAN_1);
        addQuestion(q3);
        Question q4 = new Question("Kraton", "kraton", 4, Question.BAGIAN_1);
        addQuestion(q4);
        Question q5 = new Question("Sadewa", "sadewa", 5, Question.BAGIAN_1);
        addQuestion(q5);
        Question q6 = new Question("Ramayana", "ramayana", 6, Question.BAGIAN_1);
        addQuestion(q6);
        Question q7 = new Question("Pusaka", "pusaka", 7, Question.BAGIAN_1);
        addQuestion(q7);
        Question q8 = new Question("Menawi", "menawi", 8, Question.BAGIAN_1);
        addQuestion(q8);
        Question q9 = new Question("Rahayu", "rahayu", 9, Question.BAGIAN_1);
        addQuestion(q9);
        Question q10 = new Question("Semar", "semar", 10, Question.BAGIAN_1);
        addQuestion(q10);
        Question q11 = new Question("Gajah", "gajah", 1, Question.BAGIAN_2);
        addQuestion(q11);
        Question q12 = new Question("Arjuna", "arjuna", 2, Question.BAGIAN_2);
        addQuestion(q12);
        Question q13 = new Question("Meniko", "meniko", 3, Question.BAGIAN_2);
        addQuestion(q13);
        Question q14 = new Question("Sedoso", "sedoso", 4, Question.BAGIAN_2);
        addQuestion(q14);
        Question q15 = new Question("Janaka", "janaka", 5, Question.BAGIAN_2);
        addQuestion(q15);
        Question q16 = new Question("Gelang", "gelang", 6, Question.BAGIAN_2);
        addQuestion(q16);
        Question q17 = new Question("Wijaya", "wijaya", 7, Question.BAGIAN_2);
        addQuestion(q17);
        Question q18 = new Question("Adipati", "adipati", 8, Question.BAGIAN_2);
        addQuestion(q18);
        Question q19 = new Question("Kretek", "kretek", 9, Question.BAGIAN_2);
        addQuestion(q19);
        Question q20 = new Question("Telaten", "telaten", 10, Question.BAGIAN_2);
        addQuestion(q20);
        Question q21 = new Question("Barang Apik", "barang apik", 11, Question.BAGIAN_2);
        addQuestion(q21);
        Question q22 = new Question("Nandur Pari", "nandur pari", 12, Question.BAGIAN_2);
        addQuestion(q22);
        Question q23 = new Question("Kebon Gedhang", "kebon gedhang", 13, Question.BAGIAN_2);
        addQuestion(q23);
        Question q24 = new Question("Gorengan Anget", "gorengan anget", 14, Question.BAGIAN_2);
        addQuestion(q24);
        Question q25 = new Question("Sinau Sregep", "sinau sregep", 15, Question.BAGIAN_2);
        addQuestion(q25);
    }

    private void addQuestion(Question question){
        ContentValues cv = new ContentValues();
        cv.put(DBContract.DBTable.COLUMN_QUESTION, question.getQuestion());
        cv.put(DBContract.DBTable.COLUMN_ANSWER, question.getAnswer());
        cv.put(DBContract.DBTable.COLUMN_ANSWER_NR, question.getAnswer_nr());
        cv.put(DBContract.DBTable.COLUMN_BAGIAN, question.getBagian());
        db.insert(DBContract.DBTable.TABLE_NAME, null, cv);
    }

    public List<Question> getAllQuestion() {
        List<Question> questionList = new ArrayList<>();
        db = getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + DBContract.DBTable.TABLE_NAME, null);

        if (c.moveToFirst()){
            do {
                Question question = new Question();
                question.setQuestion(c.getString(c.getColumnIndex(DBContract.DBTable.COLUMN_QUESTION)));
                question.setAnswer(c.getString(c.getColumnIndex(DBContract.DBTable.COLUMN_ANSWER)));
                question.setAnswer_nr(c.getInt(c.getColumnIndex(DBContract.DBTable.COLUMN_ANSWER_NR)));
                question.setBagian(c.getString(c.getColumnIndex(DBContract.DBTable.COLUMN_BAGIAN)));
                questionList.add(question);
            }while (c.moveToNext());
        }
        c.close();
        return questionList;
    }

    public List<Question> getQuestion(String bagian) {
        List<Question> questionList = new ArrayList<>();
        db = getReadableDatabase();

        String[] selectionArgs = new String[]{bagian};
        Cursor c = db.rawQuery("SELECT * FROM " + DBContract.DBTable.TABLE_NAME +
                " WHERE " + DBContract.DBTable.COLUMN_BAGIAN + " = ?", selectionArgs);

        if (c.moveToFirst()){
            do {
                Question question = new Question();
                question.setQuestion(c.getString(c.getColumnIndex(DBContract.DBTable.COLUMN_QUESTION)));
                question.setAnswer(c.getString(c.getColumnIndex(DBContract.DBTable.COLUMN_ANSWER)));
                question.setAnswer_nr(c.getInt(c.getColumnIndex(DBContract.DBTable.COLUMN_ANSWER_NR)));
                question.setBagian(c.getString(c.getColumnIndex(DBContract.DBTable.COLUMN_BAGIAN)));
                questionList.add(question);
            }while (c.moveToNext());
        }
        c.close();
        return questionList;
    }
}
