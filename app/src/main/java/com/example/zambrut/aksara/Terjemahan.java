package com.example.zambrut.aksara;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Terjemahan extends Activity {
    TextView teks, teks2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_terjemahan);
        teks=(TextView)findViewById(R.id.aksara);
        final EditText inputText = (EditText) findViewById(R.id.input);
        Button btTampil = (Button) findViewById(R.id.terjemahan);
        final TextView tampilText = (TextView) findViewById(R.id.aksara);
        Typeface customfont=Typeface.createFromAsset(getAssets(),"fonts/aksara.TTF");
        teks.setTypeface(customfont);

        btTampil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String input = inputText.getText().toString();
                tampilText.setText(input);
            }
        });
    }
    public void kembali(View view) {
        finish();
    }

}
