package com.example.zambrut.aksara;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Collections;
import java.util.List;

public class Bagian_1 extends Activity {
    private TextView textQuestion, teks2, textQuestionCount, textAnswer;
    private Button btnLanjutkan;
    private EditText edtAnswer;

    private List<Question> questionList;
    private int questionCounter;
    private int questionCountTotal;
    private Question currentQuestion;

    private boolean answered;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_bagian_1);

//        teks=(TextView)findViewById(R.id.texta);
//        Typeface customfont=Typeface.createFromAsset(getAssets(),"fonts/aksara.TTF");
//        teks.setTypeface(customfont);

        textQuestion = (TextView) findViewById(R.id.text_question);
        Typeface customfont = Typeface.createFromAsset(getAssets(), "fonts/aksara.TTF");
        textQuestion.setTypeface(customfont);
        textQuestionCount = (TextView) findViewById(R.id.text_question_count);
        btnLanjutkan = (Button) findViewById(R.id.btn_lanjutkan);
        edtAnswer = (EditText) findViewById(R.id.edt_answer);

        DBHelper dbHelper = new DBHelper(this);
        questionList = dbHelper.getQuestion("1");
        questionCountTotal = questionList.size();


        btnLanjutkan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkAnswer();
            }
        });

        showNextQuestion();
    }

    private void showNextQuestion(){

        if (questionCounter < questionCountTotal){
            currentQuestion = questionList.get(questionCounter);

            textQuestion.setText(currentQuestion.getQuestion());
//            Typeface customfont = Typeface.createFromAsset(getAssets(), "fonts/aksara.TTF");

            questionCounter++;
            textQuestionCount.setText("Soal " + questionCounter + "/" + questionCountTotal);
        }else {
            finishQuiz();
        }
    }


    private void checkAnswer(){
        if (!edtAnswer.getText().toString().isEmpty()){
//            edtAnswer.setError("Isi dengan Benar");
            if (edtAnswer.getText().toString().equalsIgnoreCase(currentQuestion.getAnswer())){
                Toast.makeText(this, "Jawaban Benar", Toast.LENGTH_SHORT).show();
                showNextQuestion();
            }else {
                Toast.makeText(this, "Jawaban Salah", Toast.LENGTH_SHORT).show();
            }
        }else {
            Toast.makeText(this, "Masukkan Jawaban", Toast.LENGTH_SHORT).show();
        }
    }

    public void kembali(View view) {
        finish();
    }

    private void finishQuiz(){
        finish();
    }
}
