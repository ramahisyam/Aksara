package com.example.zambrut.aksara;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

public class Quiz extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        Button btnKekanan = (Button)findViewById(R.id.kanan);
        setContentView(R.layout.activity_quiz);

    }
    public void kembali(View view) {
        finish();
    }
    public void bagian1(View v) {
        final Animation animTranslate = AnimationUtils.loadAnimation(this, R.anim.anim_translate);
        Intent intent = new Intent(Quiz.this, Bagian_1.class);
        startActivity(intent);
       v.startAnimation(animTranslate);
    }
    public void bagian2(View v) {
        final Animation animTranslate = AnimationUtils.loadAnimation(this, R.anim.anim_translate);
        Intent intent = new Intent(Quiz.this, Bagian_2.class);
        startActivity(intent);
        v.startAnimation(animTranslate);
    }
    public void bagian3(View v) {
        final Animation animTranslate = AnimationUtils.loadAnimation(this, R.anim.anim_translate);
        v.startAnimation(animTranslate);
    }
    public void bagian4(View v) {
        final Animation animTranslate = AnimationUtils.loadAnimation(this, R.anim.anim_translate);
        v.startAnimation(animTranslate);
    }
    public void bagian5(View v) {
        final Animation animTranslate = AnimationUtils.loadAnimation(this, R.anim.anim_translate);
        v.startAnimation(animTranslate);
    }

}

