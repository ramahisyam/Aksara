package com.example.zambrut.aksara;

import android.provider.BaseColumns;

public final class DBContract {

    public static class DBTable implements BaseColumns {

        public static final String TABLE_NAME = "quiz_question";
        public static final String COLUMN_QUESTION = "question";
        public static final String COLUMN_ANSWER = "answer";
        public static final String COLUMN_ANSWER_NR = "answer_nr";
        public static final String COLUMN_BAGIAN = "bagian";
    }

}
